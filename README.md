# The Titanic Project

## Overview
This repository contains all the files needed to create a dockerized MySQL databse, containing data on the passengers of ill-fated ship Titanic,
as well as dockerized NodeJS app that provides a simple front-end to browse that data, and an API that allows a user to:
* retrieve a full list of all passengers
* retrieve the data of one particular passenger
* delete a passenger
* add a passenger
* update the data for one particular passenger

## Prerequisites
In order to build and run the Docker images, you'll need [Docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/).
If your platform supports it, you may want to run [Docker rootless](https://docs.docker.com/engine/security/rootless/), for enhanced security.
If you want to run on a Kubernetes cluster, you'll need [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) as well.

## Install and run with just Docker
Simply download or clone this repository, and use your favorite shell to cd to its root, and run these commands (you can substitute '&lt;choose-a-random-password&gt;' with anything you want)::
```
export MYSQL_ROOT_PASSWORD=<choose-a-random-password>
docker-compose up
```
You can now access the API at http://localhost:3000. If you visit that address (or http://localhost:3000/people) in your browser, you will see the simple GUI.
You can use the API interactively in your browser at http://localhost:3000/api-docs.

## Install and run on Kubernetes
* Download or clone this repository
* [Set your Docker context](https://docs.docker.com/engine/context/working-with-contexts/) to that of your Kubernetes cluster. If you're using [minikube](https://minikube.sigs.k8s.io/docs/start/) as a sandbox, you can do that with this command:
```
> eval $(minikube docker-env)
```
* Then run `docker-compose build` in order to create the images and store them in the cluster's registry.
```
docker-compose build
```
* Next, create a Kubernetes secret to hold the root password of the database, using this command (you can substitute '&lt;choose-a-random-password&gt;' with literally anything you want):
```
> kubectl create secret generic mysql-root-password --from-literal=mysql-root-password='<choose-a-random-password>'
```
* Now you can start the pods for the database and NodeJS app like this:
```
kubectl apply -f kube/db.yaml
kubectl apply -f kube/node.yaml
```
* Finally, connect to the Kubernetes service we just created for the NodeJS app by port forwarding from a local port to the service's port:
```
kubectl port-forward service/node 8080:3000
```
Note that port 8080 is used as an example here; you can substitute any port number.
You can now access the API at http://localhost:8080. If you visit that address (or http://localhost:8080/people) in your browser, you will see the simple GUI.
You can use the API interactively in your browser at http://localhost:8080/api-docs.

## Notes on development
The "Titanic Project" was created as part of an assignment. The author was provided with instructions, the swagger.yml file describing the API, and a csv file containing the passenger data.

### NodeJS App
I chose to start by using the [OpenAPI code generator](https://openapi-generator.tech/) to generate an API server in NodeJS/Express, because that is a stack I'm familiar with. 
As it turned out, the NodeJS/Express generator is in beta, and I ran into some problems:
* The generator couldn't handle the `$ref` directives in the swagger.yml correctly, so I ended up having to dereference the yml file. This does not impact the API definition in any way, as it just replaces the $ref directives with the declarations that they refer to.
* The OperationID values in the swagger.yml contained a period, which led to NodeJs syntax errors, so I replaced then with camelCase values (for example, replaced the person.get OperationId with personGet). This also does not impact the API definition in any way, as the OperationID value is an optional value and is only required to be unique.

Now that I had a working, but 'skeletal' version of the NodeJS app, I added a simple front end, which uses ajax calls to the API to display the list of passengers, and allows a user to click on a passenger to display full details on that passenger.

### MySQL database
Next, it was time to add a database. I decided to use MySQL, because I had good experiences with it in the past. I wrote a Dockerfile that pulls the latest MySQL docker base image and creates a directory in the image that is only readable my the 'mysql' user, copies the CSV containing the passenger data into that directory, and then adds the --secure-file-priv option to the MySQL config, so that it can use that dir to load data from. I also wrote a SQL file that creates the 'PERSON' table, loads the data from the specified directory, and defines 5 stored procedures that can be used to get a full list of all people stored in the table, get the data for a specific person from the table,add a person to the table, delete a person from the table, and update the data for a specific person. This setup.sql file is added to the /docker-entrypoint-initdb.d directory in the image so that it will be executed at database startup.

At this point, I created a docker-compose.yml file, and added directives to it for creating the mysql image. Besides the name of the image and port mapping, it passed the MYSQL_ROOT_PASSWORD environment variable from the host to the image. In this way, there won't be an unsecured password in the docker-compose.yml (and therefore in any version control repositories), but it will obviously be present in the command history of the host. That's one of many reasons for the host to be access-restricted, along with the 
fact that anyone who has access to the host can gain access to the docker containers running on it.
I also added a directive to the docker-compose.yml file that creates a docker volume that is mapped to the /var/lib/mysql directory in the image. This ensures that our data is persistent as long as the docker volume exists.

I was now able to run the MySQL image with `docker-compose up`, and so use [DBeaver](https://dbeaver.io/) to test whether the data is loaded correctly, and the stored procedures work as expected.

### Dockerizing the NodeJS App
Next, I modified the NodeJS app generated earlier to connect to the MySQL database (DatabaseHandler.js), and to take the parameters sent to the API and use them to call the MySQL stored procedures correctly (DefaultService.js). I also modified expressServer.js to redirect to the simple GUI if a request for `text/html` is made to the /people endpoint (if a request for `application/json` is made, that endpoint will return a list of all passengers).

After testing the NodeJS app, I dockerized it. I used the node:current-alpine base image, since that's based on the Alpine linux distro, and is much more light-weight than the regular base image (up to an 80% reduction in size). In the Dockerfile, I added directives to copy package.json into the image, and then run `npm install`, so that all libraries are installed, followed by copying all required files into the image. Since it's important that the NodeJS app doesn't try to access the MySQL database before that's up and running, I used [wait-for-it.sh](https://github.com/vishnubob/wait-for-it) to check the database's regular access port (3306) to see if it's up yet, and then run `node`.
This required also installing bash, since that's not present in the light-weight nodejs-alpine base image.

In the docker-compose.yml file, I added directives for creating the node image. Besides the name of the image and port mapping, it declares the dependency on the MySQL database, and passes the MYSQL_ROOT_PASSWORD environment variable from the host to the image. it also sets the DATABASE_HOST environment variable to the right hostname, so it can be used by the NodeJS app to connect to the database.

At this point, I was able to run `docker-compose up` and both the NodeJS app and the MySQL started up without problems as Docker containers.

### Kubernetes preparation
My final step was to create Kubernetes yaml files that would deploy the docker images I had created on a Kubernetes cluster, in the right way. I started with using the ['kompose' tool](https://github.com/kubernetes/kompose) to take the docker-compose.yml file as input and generate Kubernetes yaml files for deployments of both images, and for creating Kubernetes services to expose them. Instead of passing the MYSQL_ROOT_PASSWORD variable from the host, I store it in a kubectl secret, for better security, so I modified the files created by the kompose tool to reflect that; I also modified the pull policy, so that the cluster will pull the images from its associated registry, if it is not present yet. I also created a yaml file for creating a persistent volume. I then merged the various yaml files together, so there is one for the MySQL database, and one for the NodeJS app. 

In order to test both files, I used [Minikube](https://minikube.sigs.k8s.io/), and was able to get everything up and running.
