/* eslint-disable no-unused-vars */
const Service = require('./Service');
const DataBaseHandler = require("./DataBaseHandler");
const util = require("util");
var dataBaseHandler = new DataBaseHandler();
var connection = dataBaseHandler.createConnection();

/**
* Add a person to the database
*
* person PersonData 
* returns Person
* */
const peopleAdd = ({ person }) => new Promise(
  async (resolve, reject) => {
    queryStr = "CALL sp_AddPerson(?,?,?,?,?,?,?,?,@uuid); SELECT @uuid;";
    connection.query(queryStr, [person.survived, person.passengerClass, person.name, person.sex, person.age, person.siblingsOrSpousesAboard, person.parentsOrChildrenAboard, person.fare], function (error, result, fields) {
      if (error) {
        reject(Service.rejectResponse(
          error.message || 'Invalid input',
          error.status || 405,
        ));
      } else {
        try {
          uuid = result[1][0]['@uuid'];
          resolve(Service.successResponse({ uuid })); 
        } catch (e) {
          reject(Service.rejectResponse(
            e.message || 'Invalid input',
            e.status || 405,
          ));
        }
      }
    })
  },
);

/**
* Get a list of all people
*
* returns List
* */
const peopleList = () => new Promise(
  async (resolve, reject) => {
    connection.query('CALL sp_GetPeople();', function (error, result, fields) {
      if (error) {
        reject(Service.rejectResponse(
          error.message || 'Invalid input',
          error.status || 405,
        ));
      } else {
        try {
          resolve(Service.successResponse( result[0] ));
        } catch (e) {
          reject(Service.rejectResponse(
            e.message || 'Invalid input',
            e.status || 405,
          ));
        }
      }
    })
  },
);

/**
* Delete this person
*
* uuid UUID 
* no response value expected for this operation
* */
const personDelete = ({ uuid }) => new Promise(
  async (resolve, reject) => {
    connection.query('CALL sp_DeletePerson("'+ uuid +'");', function (error, result, fields) {
      if (error) {
        reject(Service.rejectResponse(
          error.message || 'Invalid input',
          error.status || 405,
        ));
      } else {
        try {
          resolve(Service.successResponse( result[0] )); // Originally 'uuid' 
        } catch (e) {
          reject(Service.rejectResponse(
            e.message || 'Invalid input',
            e.status || 405,
          ));
        }
      }
    })
  },
);

/**
* Get information about one person
*
* uuid UUID 
* returns Person
* */
const personGet = ({ uuid }) => new Promise(
  async (resolve, reject) => {
    connection.query('CALL sp_GetPerson("'+ uuid +'");', function (error, result, fields) {
      if (error) {
        reject(Service.rejectResponse(
          error.message || 'Invalid input',
          error.status || 405,
        ));
      } else {
        if (result[0].length== 0) {
          reject(Service.rejectResponse('ID not found', 404));
        } else {
          try {
            resolve(Service.successResponse( result[0][0] ));
          } catch (e) {
            reject(Service.rejectResponse(
              e.message || 'Invalid input',
              e.status || 405,
            ));
          }
        }
      }
    });
  },
);

/**
* Update information about one person
*
* uuid UUID 
* person PersonData 
* returns Person
* */
const personUpdate = ({ uuid, person }) => new Promise(
  async (resolve, reject) => {
    queryStr = "CALL sp_UpdatePerson(?,?,?,?,?,?,?,?,?);";
    connection.query(queryStr, [person.survived, person.passengerClass, person.name, person.sex, person.age, person.siblingsOrSpousesAboard, person.parentsOrChildrenAboard, person.fare, uuid], function (error, result, fields) {
      if (error) {
        reject(Service.rejectResponse(
          error.message || 'Invalid input',
          error.status || 405,
        ));
      } else {
        try {
          console.log("service.peopleUpdate successful; uuid: " + util.inspect(uuid, {showHidden: false, depth: null}));
          resolve(Service.successResponse({ uuid })); 
        } catch (e) {
          reject(Service.rejectResponse(
            e.message || 'Invalid input',
            e.status || 405,
          ));
        }
      }
    })
  },
);

module.exports = {
  peopleAdd,
  peopleList,
  personDelete,
  personGet,
  personUpdate,
};
