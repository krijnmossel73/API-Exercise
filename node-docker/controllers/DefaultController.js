/**
 * The DefaultController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic reoutes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/DefaultService');

const peopleAdd = async (request, response) => {
  await Controller.handleRequest(request, response, service.peopleAdd);
};

const peopleList = async (request, response) => {
  await Controller.handleRequest(request, response, service.peopleList);
};

const personDelete = async (request, response) => {
  await Controller.handleRequest(request, response, service.personDelete);
};

const personGet = async (request, response) => {
  await Controller.handleRequest(request, response, service.personGet);
};

const personUpdate = async (request, response) => {
  await Controller.handleRequest(request, response, service.personUpdate);
};


module.exports = {
  peopleAdd,
  peopleList,
  personDelete,
  personGet,
  personUpdate,
};
