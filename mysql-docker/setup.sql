/* 
This file contains a series of SQL commands meant to be run on mysql.
It creates a `PERSON` table, containing 8 fields plus a `personId` field
that is the primary key. 

It also loads the data contained in the '/tmp/sql-files/titanic.csv' file
into that table.

Finally, it defines 5 stored procedures that can be used to get a full list of
all people stored in the table, get the data for a specific person from the table,
add a person to the table, delete a person from the table, and update the data
for a specific person.
*/

CREATE TABLE PERSON (
  survived                BOOLEAN                         NULL,
  passengerClass          INTEGER                         NULL,
  name                    VARCHAR(100)                    NULL,
  sex                     ENUM('female', 'male', 'other') NULL,
  age                     INTEGER                         NULL,
  siblingsOrSpousesAboard INTEGER                         NULL,
  parentsOrChildrenAboard INTEGER                         NULL,
  fare                    FLOAT                           NULL,
  personId                VARCHAR(36)                     PRIMARY KEY
)
  ENGINE = INNODB;

/* INSERT DATA */
LOAD DATA INFILE '/tmp/sql-files/titanic.csv'
INTO TABLE PERSON
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare)
SET personId = uuid();

DROP PROCEDURE IF EXISTS sp_GetPeople;
DROP PROCEDURE IF EXISTS sp_GetPerson;
DROP PROCEDURE IF EXISTS sp_AddPerson;
DROP PROCEDURE IF EXISTS sp_DeletePerson;
DROP PROCEDURE IF EXISTS sp_UpdatePerson;

DELIMITER //

CREATE PROCEDURE sp_AddPerson(
  IN survived BOOLEAN,
  IN passengerClass INTEGER,
  IN name VARCHAR(100),
  IN sex ENUM('female', 'male', 'other'),
  IN age INTEGER,
  IN siblingsOrSpousesAboard INTEGER,
  IN parentsOrChildrenAboard INTEGER,
  IN fare FLOAT,
  OUT uuid VARCHAR(36)
)
BEGIN
  SET uuid = uuid();
  INSERT INTO PERSON
  VALUES(survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare, uuid);
END //

CREATE PROCEDURE sp_UpdatePerson(
  IN survived BOOLEAN,
  IN passengerClass INTEGER,
  IN name VARCHAR(100),
  IN sex ENUM('female', 'male', 'other'),
  IN age INTEGER,
  IN siblingsOrSpousesAboard INTEGER,
  IN parentsOrChildrenAboard INTEGER,
  IN fare FLOAT,
  IN wantedId VARCHAR(36)
)
BEGIN
  UPDATE PERSON
  SET survived=survived, passengerClass=passengerClass, name=name, sex=sex, age=age, siblingsOrSpousesAboard=siblingsOrSpousesAboard, parentsOrChildrenAboard=parentsOrChildrenAboard, fare=fare
  WHERE personId = wantedId;
END //

CREATE PROCEDURE sp_GetPeople()
BEGIN
    SELECT * FROM PERSON;
END //

CREATE PROCEDURE sp_GetPerson(
  IN wantedId VARCHAR(36)
)
BEGIN
  SELECT * 
  FROM PERSON
  WHERE personId = wantedId;
END //

CREATE PROCEDURE sp_DeletePerson(
  IN wantedId VARCHAR(36)
)
BEGIN
  DELETE 
  FROM PERSON
  WHERE personId = wantedId;
END //

DELIMITER ;
